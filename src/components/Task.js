import React, { Component } from "react"; 
import logo from '../logo.svg';
import { getCurrentUser, logout, post, get } from "../api";
import TodoList from './TodoList';

class TaskManager extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            userName: '',
            email: '',
            _id: '',
            tasks: []
        }
        this.task = {}

        this.url = 'https://aneh-sekalian.herokuapp.com/api/v1/tasks';

        get(`${this.url}?page=1&limit=10`).then(data => {
            this.setState({
                tasks: data.data.data.docs
            })
        }).catch(err=> {})
        
    }

    UNSAFE_componentWillMount() {
        getCurrentUser().then(res => {
            this.setState({
                userName: res.data.data.name,
                email: res.data.data.email,
                _id: res.data.data._id
            })
        }).catch(err => {
            alert('You must login to continue!');
            window.location.href = '/'
        })
    }

    handleSubmit = (e) => {

        if (this._name.value !== '' && this._description.value !== '' && this._deadline.value !== '') {
            this.task = {
                name: this._name.value,
                description: this._name.value,
                deadLine: this._deadline.value
            }
    
            let newTask = this.state.tasks
            newTask.push(this.task)

            post (this.url, this.task).then(res => {
                alert('Added task')

                this.setState({
                    tasks: newTask
                })
            })
            .catch(err => {
                console.log(err)
            })
    
            this._name.value = ''
            this._description.value = ''
            this._deadline.value = ''
    

        } else {
            alert('Inputs must not be empty!')
        }

    }

    handleLogout = () => {
        logout()
    }

  render() {
    return (
     <div className="dashboard">
       <div className="topbar">
            <div className="banner-badge">
                Todos
            </div>
            <div className="logout-btn" onClick={this.handleLogout.bind(this)}>
                Sign Out
            </div>
       </div>
       <div className="content">
           <div className="logo-tab">
            <img src={ logo } alt="display-logo" width="70px" />
           </div>
           <div className="profile-tab">
            <div className="todo-menu">
                <strong>{ this.state.userName }</strong>
            </div>
            <div className="todo-menu">
                My Day
            </div>
            <div className="todo-menu">
                Important
            </div>
            <div className="todo-menu">
                Completed
            </div>
           </div>
           <div className="todo-tab">
               <div className="todo-input-box">
                   <div>
                        <input name="name" className="todo-input form-control" placeholder="Task Name" ref={ a => this._name = a}/>
                   </div>
                   <div>
                    <input name="description" className="todo-input form-control" placeholder="Task description" ref={ a => this._description = a }/>
                   </div>
                    <div>
                        <input type="date" name="deadline" className="todo-input form-control" ref={a => this._deadline = a} />
                    </div>
                    <button className="add-task" onClick={this.handleSubmit.bind(this)}>+</button>
               </div>

               <div className="todo-list">
                <TodoList items={this.state.tasks} />
               </div>
           </div>
       </div>
     </div>
    );
  }
}

export default TaskManager;