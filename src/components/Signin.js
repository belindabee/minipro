import React, { Component } from "react"; 
import { Link } from 'react-router-dom';
import { authLogin, getCurrentUser } from '../api'
import { css } from '@emotion/core'
import { ScaleLoader } from 'react-spinners'

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
  left: 50%
`;

class Signin extends Component {

    constructor(...args) {
        super(...args);

        this.state = {
            email: '',
            password: '',
            loading: false
        }
    }

    UNSAFE_componentWillMount() {
        getCurrentUser().then(res => { window.location.href= '/todo' }).catch(err => { })
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = () => {

        this.setState({
            loading: true
        })

        const url = 'https://aneh-sekalian.herokuapp.com/api/v1/users';
        const data = this.state;

        authLogin(url, data)
        .then(res => {
            alert('Logged in');
            localStorage.setItem('token', res.data.data)

            window.location.href = '/todo';
        })
        .catch(err => {
            this.setState({
                loading: false
            })
            alert('Failed to log in')
        })
    }
  render() {
    return (
     <div className="auth-page">
       <div className="side-buttons">
           <div className="banner-badge">
               Todos
           </div>
           <div className="main-title">
               Hello, Friend!
           </div>
           <div className="subtitle-2">
            Enter your personal details and
           </div>
           <div className="subtitle-1">
           start your journey with us 
           </div>
            <Link to="/">
                <button className="sign-btn" >
                    SIGN UP
                </button>
            </Link>
       </div>

       <div className="main-header">
           Sign In to Task Manager
       </div>
       <button className="socmed-1">
           <i className="icon fa fa-facebook"></i>
       </button>
       <button className="socmed-2">
           <i className="icon fa fa-google-plus"></i>
       </button>
       <button className="socmed-3">
           <i className="icon fa fa-linkedin"></i>
       </button>

       <div className="main-subtitle">
        or use your email account
       </div>

       <div className="input-group">
           <input name="email" type="email" placeholder="Email" className="form-control" onChange={ this.handleChange.bind(this) }/>
           <input name="password" type="password" placeholder="Password" className="form-control" onChange={ this.handleChange.bind(this) }/>
       </div>

       <button className="submit-btn" onClick={ this.handleSubmit.bind(this) }>
           SIGN IN
       </button>

       <ScaleLoader 
          css={override}
          height={100}
          width={50}
          margin={3}
          color={"white"}
          loading={this.state.loading}
        />
     </div>
    );
  }
}

export default Signin;