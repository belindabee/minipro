import React, { Component } from "react"; 

class TodoList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: this.props.items
        }
    }

    createTasks(item, index) {
        return <li key={index}>
                    <div>{item.name}</div>
                    <div>{item.description}</div>
                    <div>{item.deadLine}</div>
                </li>;
    }

    render() {

        var todoEntries = this.props.items;
        var listItems = todoEntries.map(this.createTasks)
    return (
     <div>
         <ul>
             { listItems }
         </ul>

     </div>
    );
  }
}

export default TodoList;