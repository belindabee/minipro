import React, { Component } from "react"; 
import { Link } from 'react-router-dom';
import { register, getCurrentUser } from "../api";
import { css } from '@emotion/core'
import { ScaleLoader } from 'react-spinners'

const override = css`
  display: block;
  margin: 0 auto;
  vertical-align: middle;
  top: 50%;
  border-color: red;
`;

class Signup extends Component {

    constructor(...args) {
        super(...args);

        this.state = {
            name: '',
            email: '',
            password: '',
            loading: false
        }
    }

    UNSAFE_componentWillMount() {
        getCurrentUser().then(res => { window.location.href= '/todo' }).catch(err => { })
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = (e) => {
        this.setState({
            loading: true
        });

        const url = 'https://aneh-sekalian.herokuapp.com/api/v1/users';
        const data = this.state

        register(url, data)
        .then(res => {
            alert('Created')
            window.location.href = '/signin'
        }).catch(err => {
            alert('Failed to create')

            this.setState({
                loading: false
            });
        })
    }

  render() {
    return (
     <div className="auth-page">
       <div className="side-buttons">
           <div className="banner-badge">
               Todos
           </div>
           <div className="main-title">
               Welcome Back!
           </div>
           <div className="subtitle-1">
            login with your personal info
           </div>
           <div className="subtitle-2">
            To keep connected with us please
           </div>
            <Link to="/signin">
                <button className="sign-btn">
                    SIGN IN
                </button>

            </Link>
       </div>

       <div className="main-header">
           Create Account
       </div>
       <button className="socmed-1">
           <i className="icon fa fa-facebook"></i>
       </button>
       <button className="socmed-2">
           <i className="icon fa fa-google-plus"></i>
       </button>
       <button className="socmed-3">
           <i className="icon fa fa-linkedin"></i>
       </button>

       <div className="main-subtitle">
        or use your email for registration
       </div>

       <div className="input-group">
           <input name="name" placeholder="Name" className="form-control" onChange={ this.handleChange.bind(this)} />
           <input name="email" type="email" placeholder="Email" className="form-control" onChange={ this.handleChange.bind(this) }/>
           <input name="password" type="password" placeholder="Password" className="form-control" onChange={ this.handleChange.bind(this) } />
       </div>

       <ScaleLoader 
          css={override}
          height={100}
          width={50}
          margin={3}
          color={"white"}
          loading={this.state.loading}
        />

       <button className="submit-btn" onClick={this.handleSubmit.bind(this)}>
           SIGN UP
       </button>
     </div>
    );
  }
}

export default Signup;