import React from 'react';
import './style.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Signin from './components/Signin';
import Signup from './components/Signup';
import TaskManager from './components/Task';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Signup} />
          <Route path="/signin" component={Signin} />
          <Route path="/todo" component={TaskManager} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
