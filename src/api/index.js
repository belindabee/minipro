import axios from 'axios'

export const authLogin = (url,data) => {
    return axios({
        method : 'PUT',
        headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
        url : url,
        data : data
    })
}

export const register = (url, data) => {
    return axios({
        method: 'POST',
        headers: {
            'Accept': 'Application/json',
        },
        url: url,
        data: data
    })
}

export const getCurrentUser = () => {
    return axios({
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': `${localStorage.getItem('token')}`
        },
        url: 'https://aneh-sekalian.herokuapp.com/api/v1/users'
    })
}

export const get = (url) => {
    return axios({
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Authorization': `${localStorage.getItem('token')}`
        },
        'url': url
    })
}

export const put = (url, data) => {
    return axios({
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': `${localStorage.getItem('token')}`
        },
        'url': url,
        'data': data
    })
}

export const post = (url, data) => {
    return axios({
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Authorization': `${localStorage.getItem('token')}`
        },
        'url': url,
        'data': data
    })
}

export const logout = () => {
    localStorage.removeItem('token');
    alert('Logout successfully')

    window.location.href = '/'
}